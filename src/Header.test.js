import React from "react";
import ReactDOM from "react-dom";
import { shallow, mount } from "enzyme";
// import { expect } from "chai";
import Header from "./Components/Header";

describe("Header", () => {
  it("renders Header", () => {
    const wrapper = shallow(<Header />);

    expect(wrapper.find(".header_logo")).toHaveLength(1);
  });
});
